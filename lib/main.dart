import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

//this import is for the custom icon ZSD
import 'package:navigation/socicon_icons.dart';


//Lab 2, bottom navigation/ bottom sheet

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      debugShowCheckedModeBanner: false,
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int index = 0;

  //navigate to the Dogs() screen ZSD
  var tabs = [Dogs(), Text('Cats'), Text('Profile')];

  @override
  Widget build(BuildContext context) {
    //implement bottom navigation
    return Scaffold(
        appBar: AppBar(
          title: Text('Lab 2'),
        ),
        //displays the element in tabs
        body: Center(child: tabs.elementAt(index)),
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          backgroundColor: Colors.blue,
          selectedItemColor: Colors.white,
          unselectedItemColor: Colors.white.withOpacity(.50),
          selectedFontSize: 12,
          unselectedFontSize: 12,
          currentIndex: index,
          onTap: (value) => setState(() {
            index = value;
          }),
          items: [
            //create each tab
            BottomNavigationBarItem(
                title: Text('Dogs'),
                //using custom icon for dog ZSD
                icon: Icon(Socicon.dog)),
            BottomNavigationBarItem(
                title: Text('Cats'), icon: Icon(Icons.photo)),
            BottomNavigationBarItem(
                title: Text('Profile'), icon: Icon(Icons.person))
          ],
        ));
  }
}

/// Dogs class ZSD
/// Navigate to a new screen using the Dog profile icon
class Dogs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        //background color
          backgroundColor: Colors.amber[300],

          //appBar to match background somewhat
          appBar: AppBar(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Dogs Love To Play", style: TextStyle(
                  color: Colors.blue,
                  fontSize: 30
                ),),
              ],
            ),
            backgroundColor: Colors.amber[300],
            shadowColor: Colors.white,
            elevation: 8,
          ),

          //Page itself
          body: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              //Expanded used to get all the pictures to fit properly
              Expanded(

                //First Row
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      padding: EdgeInsets.all(1.0),
                      child: Image(
                        image: AssetImage('assets/dog1.jpg'),
                      ),
                    ),
                    Container(
                        padding: EdgeInsets.all(1.0),
                        child: Image(
                          image: AssetImage('assets/dog2.jpg'),
                        )),
                  ],
                ),
              ),
              Expanded(

                //Second Row
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      padding: EdgeInsets.all(1.0),
                      child: Image(
                        image: AssetImage('assets/dog3.jpg'),
                      ),
                    ),
                    Container(
                        padding: EdgeInsets.all(1.0),
                        child: Image(
                          image: AssetImage('assets/dog4.jpg'),
                        )),
                  ],
                ),
              ),
              Expanded(

                //Third Row
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      padding: EdgeInsets.all(1.0),
                      child: Image(
                        image: AssetImage('assets/dog5.jpg'),
                      ),
                    ),
                    Container(
                        padding: EdgeInsets.all(1.0),
                        child: Image(
                          image: AssetImage('assets/dog6.jpg'),
                        )),
                  ],
                ),
              ),
            ],
          )),
    );
  }
}
